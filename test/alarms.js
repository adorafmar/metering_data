var fs = require('fs');
var expect = require("chai").expect;
var alarms = require("../app/alarms");
var PassThrough = require('stream').PassThrough;
var sinon = require('sinon');
var http = require('http');

//     {
//       "cleared_data": {
//         "time": 1549987596093
//       },
//       "alarming": false,
//       "acknowledged": false,
//       "name": "Alarm",
//       "last_event_state": {
//         "intValue": 1
//       },
//       "uuid": "6e7890ec-4bb1-4ce4-be24-4c489a9b856b",
//       "active_data": {
//         "setpointA": "0.0",
//         "mode": "Not Equal",
//         "eventValue": "True",
//         "time": 1549987591631
//       },
//       "cleared": true,
//       "who": "prov:default:/tag:TSG_KW/TSG_KW/E0713/0Error:/alm:Alarm"
//     }

function IGNITION_TAG(options) {
    var tag = [];
    tag.push(options['alarming']     || 'false'); // machine
    tag.push(options['acknowledged'] || 'false'); // operator
    tag.push(options['when_alarmed'] || "Mon Feb 26 05:36:02 CST 2018"); // machine
    tag.push(options['who']          || "GEA/bldgout/Alarm"); // machine
    tag.push(options['why_alarming'] || "{mode=Not Equal,eventValue=true,activePipeline=SysErr/SysErrActive,name=Alarm,eventTime=Mon Feb 26 05:36:02 CST 2018}"); // machine
    tag.push(options['uuid']         || "cb55c19f-2f7f-4848-ac11-a011210558e7"); // machine
    tag.push(options['shelved']      || 'false'); // operator
    tag.push(options['escalated']    || 'false'); // operator
    tag.push(options['when_cleared'] || "Mon Feb 26 06:36:02 CST 2018"); // machine
    tag.push(options['ignored']      || 'false'); // operator
    return tag;
}

function HISTORY_EVENT(options){
    var columns = [ 
        'getSource',
        'getActiveData',
        'getAckData',
        'getClearedData',
        'getNotes',
        'getId' 
    ];
    var row = [];
    row.push(options['getSource'] || 'prov:default:/tag:GDC/SysErr:/alm:Alarm'); // machine
    row.push(options['getActiveData'] || 'None'); // machine
    row.push(options['getAckData'] || 'None'); // machine
    row.push(options['getClearedData'] || 'None'); // machine
    row.push(options['getNotes'] || 'notes go here'); // machine
    row.push(options['getId'] || 'cc22ff84-142f-4d87-8bae-e988206a100d'); // machine

    return {columns: columns, rows: [row]}
}

describe.skip("alarms_for_management_table", function() {
    beforeEach(function() {
        this.get = sinon.stub(http, 'get');
    });
    afterEach(function() {
        http.get.restore();
    });

    it.skip("should latest alarms", function(done) {

    });

    it.skip("should sound the siren when (alarming && !acknowledged)", function(done) {
        var tags = [];
        var options = {};
        options['alarming'] = 'true';
        options['acknowledged'] = 'false';
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);

        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(1);
            expect(data[0].alarming).equal(true);
            expect(data[0].acknowledged).equal(false);
            expect(data[0].should_siren).equal(true);

            done();
        }

        alarms.alarms_for_management_table(callback); 
    });

    it.skip("should not sound the siren when (alarming && acknowledged)", function(done) {
        var tags = [];
        var options = {};
        options['alarming'] = 'true';
        options['acknowledged'] = 'true';
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);

        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(1);
            expect(data[0].alarming).equal(true);
            expect(data[0].acknowledged).equal(true);
            expect(data[0].should_siren).equal(false);

            done();
        }

        alarms.alarms_for_management_table(callback); 
    });

    it.skip("should sound the siren when (!alarming && escalated)", function(done) {
        var tags = [];
        var options = {};
        options['alarming'] = 'false';
        options['escalated'] = 'true';
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);

        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(1);
            expect(data[0].alarming).equal(false);
            expect(data[0].escalated).equal(true);
            expect(data[0].should_siren).equal(true);

            done();
        }

        alarms.alarms_for_management_table(callback); 
    });

    it.skip("should not choke on parsing when ignition trial expires", function(done) {
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify("crap from ignition <=> }: blah blah blah} yada yada yada |}:"));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);
        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(0);

            done();
        }
        
        alarms.alarms_for_management_table(callback); 
    });

    it("should discover six tags", function(done) {
        var tags = [];

        var tag = {};
        tag['alarming'] = 'true';
        tag['who'] = "ADH/0348_A0001_PD/Hi";
        // var tag = IGNITION_TAG(tag);
        tags.push(tag);

        var tag = {};
        tag['alarming'] = 'true';
        tag['who'] = "GEA/0347_A0001_PD/Hi";
        // var tag = IGNITION_TAG(tag);
        tags.push(tag);

        var tag = {};
        tag['alarming'] = 'true';
        tag['who'] = "GEA/0347_A0001_PD/Lo";
        // var tag = IGNITION_TAG(tag);
        tags.push(tag);

        var tag = {};
        tag['alarming'] = 'true';
        tag['who'] = "GEA/0347_A0001_FLOW/Hi";
        // var tag = IGNITION_TAG(tag);
        tags.push(tag);

        var tag = {};
        tag['alarming'] = 'true';
        tag['who'] = "GEA/0347_A0001_FLOW/HiHi";
        // var tag = IGNITION_TAG(tag);
        tags.push(tag);

        var tag = {};
        tag['alarming'] = 'true';
        tag['who'] = "GEA/0347_A0001_FLOW/Lo";
        // var tag = IGNITION_TAG(tag);
        tags.push(tag);

        var tags_from_ignition = tags;
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);
        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(6);

            done();
        }
        alarms.alarms_for_management_table(callback);
    });

    it("should discover no alarms", function(done) {
        var time0 = Date.now();
        var options = {};
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);
        function callback(data){

            expect(data).to.be.a('array');
            expect(data.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });

    it("should discover one alarm", function(done) {
        var time0 = Date.now();
        var tag = {};
        tag['alarming'] = 'true';
        // var tag = IGNITION_TAG(options);

        var tags = [tag];
        var tags_from_ignition = tags;
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition);
        function callback(data){
            expect(data).to.be.a('array');
            expect(data.length).to.equal(1);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });

    it("doesn't crash when ignition doesn't send a building with the tag", function(done) {
        var options = {};
        options['who'] = 'blah blah blah';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();

        this.get.callsArgWith(1, response_from_ignition);

        alarms.alarms_for_management_table(function(result) {

            expect(result.length).to.equal(0);

            done();
        });
    });
});



describe("building_plc_meter_acquisition", function() {
    beforeEach(function() {
        this.get = sinon.stub(http, 'get');
    });
    afterEach(function() {
        http.get.restore();
    });

    it("case 1", function(done) {
        var outer_scope = 123;
        var data = fs.readFile('data.json', 'utf-8', (err, data) => { 
            if (err) throw err; 
            tags_from_ignition = JSON.parse(data);

            var response_from_ignition = new PassThrough();
            response_from_ignition.write(JSON.stringify(tags_from_ignition));
            response_from_ignition.end();
            this.get.callsArgWith(1, response_from_ignition);

            function callback(data){
                debugger;
                
                expect(data).to.be.a('array');
                expect(data.length).to.equal(1);
                expect(data[0].alarming).equal(true);
                expect(data[0].acknowledged).equal(false);
                expect(data[0].should_siren).equal(true);

                done();
            }

            alarms.alarms_for_management_table(callback); 
        })         
    });
});