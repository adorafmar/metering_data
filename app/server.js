var fs = require('fs');
var express = require('express');
var app = express(); 
var alarms = require("./alarms");
const querystring = require("querystring");
var https = require('https');
var port = require('./../config.js').get(process.env.NODE_ENV)['port'];
app.set('port', port);
const nocache = require('nocache');

app.use(nocache());

app.get('/', function (req, res) {
	res.header('Access-Control-Allow-Origin' , '*');
	res.send('Server is on line');
})

app.get('/variance', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.variance(callback); 
})

app.post('/acknowledge', function (req, res) {
	  var body = '';
	  var received_from_operator;
      req.on("data", data => {
        body += data;
      });
      req.on("end", () => {
      	received_from_operator = querystring.parse(body)
		alarms.alarms_acknowledge(received_from_operator, callback); 
      });
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
})

app.get('/alarms_for_management_table', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	var column_to_sort = req.query.column_to_sort;
	if(typeof req.query.sort_ascending != 'undefined'){
		var sort_ascending = JSON.parse(req.query.sort_ascending);
	}
	alarms.alarms_for_management_table(column_to_sort, sort_ascending, callback); 
})

app.get('/alarms_for_public_table', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_public_table(callback); 
})

app.get('/alarms_for_management_map', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_management_map(callback); 
})

app.get('/alarms_for_public_map', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_public_map(callback); 
})

app.get('/alarms_for_history', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_history(callback); 
})

app.get('/tag_history', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	var tag_needing_history = req.query.tag_needing_history;
	alarms.tag_history(tag_needing_history, callback); 
})

app.post('/login', function (req, res) {
	var body_received = '';
	req.on("data", data => {
		body_received += data;
	});
	req.on("end", () => {
		credentials = querystring.parse(body_received);
		alarms.login(credentials);
		res.header('Access-Control-Allow-Origin' , '*');
		var body_sent = JSON.stringify('GOTIT'); 
		res.send(body_sent); 
	});
});
var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('http happens on port ' + port);
});

// var ports = 4443
// https.createServer({
//   key:  fs.readFileSync('id_rsa'),
//   cert: fs.readFileSync('outagemap_utilities_utexas_edu_cert.cer')
// }, app)
// .listen(ports, function () {
//   console.log('https happens on %d', ports)
// });