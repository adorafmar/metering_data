const http = require("http");
const querystring = require("querystring");
const debug = require('debug')('tags');
var config = require('../config.js').get(process.env.NODE_ENV);
console.log("DEBUG: " + process.env.DEBUG);
console.log("NODE_ENV: " + process.env.NODE_ENV);
const sql = require("mssql");
const db_config = {
    user: 'iamtradmin',
    password: '',
    server: '10.101.206.106', 
    database: 'UT' 
};

var user_name = "";
var password = "";
var ignition_machine = config.ignition_machine;
var ignition_port = 8088;

var ignition_alarms_path  = "/system/webdev/IAMeters/alarms";
var ignition_ignore_path  = "/system/webdev/IAMeters/ignore";
var ignition_shelve_path  = "/system/webdev/IAMeters/shelve";
var ignition_escalate_path  = "/system/webdev/IAMeters/escalate";
var ignition_deescalate_path  = "/system/webdev/IAMeters/deescalate";
var ignition_history_path = "/system/webdev/IAMeters/history";
var tag_history_path = "/system/webdev/IAMeters/tag_history";

// var ignition_alarms_path  = "/main/system/webdev/IAMeters/alarms";
// var ignition_ignore_path  = "/main/system/webdev/IAMeters/ignore";
// var ignition_shelve_path  = "/main/system/webdev/IAMeters/shelve";
// var ignition_escalate_path  = "/main/system/webdev/IAMeters/escalate";
// var ignition_deescalate_path  = "/main/system/webdev/IAMeters/deescalate";
// var ignition_history_path = "/main/system/webdev/IAMeters/history";
// var tag_history_path = "/main/system/webdev/IAMeters/tag_history";

var ignition_alarms_uri  = "http://" + user_name + ":" + password + "@" + ignition_machine+":"+ignition_port+ignition_alarms_path; 
var ignition_history_uri = "http://" + user_name + ":" + password + "@" + ignition_machine+":"+ignition_port+ignition_history_path; 
var tag_history_uri = "http://" + ignition_machine+":"+ignition_port+tag_history_path; 

console.log("ignition_alarms_uri from " + ignition_alarms_uri);
console.log("ignition_history_uri from " + ignition_history_uri);

exports.variance = function(callback) {
    var n, Sx, Sy, Sx2, Sxy;
    var energies_for_campus;
    var energy_for_buildings;
    var temperatures;
    var g_models;
    sql.connect(db_config)
    .then(get_connection)
    .then(get_energies_for_campus)
    .then(get_energies_for_buildings)
    .then(get_temperatures)
    .then(get_models)
    // .then(get_predictions)
    // .catch(doh)
    function get_connection(connection){
        request = connection.request()
        return request
    }
    function get_energies_for_campus(request){
        var energy_for_campus = request.query("SELECT utility, sum(energy) as 'energy' FROM [UT].[dbo].[energies] GROUP BY utility");
        return energy_for_campus;
    }
    function get_energies_for_buildings(p_energies_for_campus){
        energies_for_campus = p_energies_for_campus;
        var energy_for_buildings = request.query("SELECT utility, building, sum(energy) as 'energy' FROM [UT].[dbo].[energies] GROUP BY utility, building");
        return energy_for_buildings;
    }
    function normalize_energies(energies){
        energies.forEach(function(energy){
            // var when = new Date(energy.when);
            // var year = when.getFullYear()
            // var month = when.getMonth();
            // var day = when.getDate();
            // when = (new Date(year, month, day)).getTime();
            // energy.when = when;

            energy.energy = Number(energy.energy);
        });
        return energies;
    }
    function get_temperatures(p_energies_for_buildings){
        energy_for_buildings = p_energies_for_buildings;
        normalize_energies(energies_for_campus);
        normalize_energies(energy_for_buildings);
        var temperatures = request.query('SELECT * FROM [UT].[dbo].[temperatures]');
        return temperatures;
    }
    function group_by_utilities(energy_for_buildings){
        var new_energy_for_buildings = {};
        energy_for_buildings.forEach(function(energy_for_building){
            var utility = energy_for_building.utility;
            var new_energy_for_building = new_energy_for_buildings[utility];
            if(new_energy_for_building === undefined){
                new_energy_for_building = [];
            }
            new_energy_for_building.push(energy_for_building);
            new_energy_for_buildings[utility] = new_energy_for_building;
        });
        return new_energy_for_buildings;
    }
    function get_models(p_temperatures){
        temperatures = p_temperatures;
        normalize_temperatures(temperatures);
        energy_for_buildings = group_by_utilities(energy_for_buildings);
        callback([temperatures, energies_for_campus, energy_for_buildings]);
    }
    function get_predictions(models, temperatures){
        var cooling_base_temperature = 0; // 57.1;
        var predictions = [];
        for(model of models){
            var prediction = {};
            model.energies_for_building.sort(function(a, b){
                return a.when - b.when
            });
            var nDays = model.energies_for_building.length;
            var record_ppd = model.energies_for_building[nDays -2];
            var record_pd  = model.energies_for_building[nDays -1];

            prediction.when = record_pd.when;
            prediction.who = record_pd.who;
            prediction.utility = record_pd.utility;
            prediction.energy_ppd = record_ppd.measurement;
            prediction.energy_pd = record_pd.measurement;
            prediction.variance = record_pd.measurement - record_ppd.measurement;
            prediction.predicted = model.b + model.m * Math.max(0, record_pd.temperature - cooling_base_temperature);

            predictions.push(prediction);
        }
        predictions.sort(function(a, b){
            return b.variance - a.variance
        });
        callback(predictions);
    }
    function normalize_temperatures(){
        temperatures.forEach(function(temperature){
            var year = temperature.when.getFullYear()
            var month = temperature.when.getMonth();
            var day = temperature.when.getDate();
            temperature.when = (new Date(year, month, day+1)).getTime();
        })
    }
    function group_energies_into_building_slash_utility(){
        var who_current = energies[0].who;
        var utility_current = energies[0].utility;
        var energy_for_buildings = [];
        var energies_for_building = [];
        for(energy of energies){
            if(energy.who == who_current && energy.utility == utility_current){
                energies_for_building.push(energy)
            } else {
                energy_for_buildings.push(energies_for_building);
                energies_for_building = [];
                energies_for_building.push(energy)
                who_current = energy.who;
                utility_current = energy.utility;
            }
        }
        energy_for_buildings.push(energies_for_building);
        return energy_for_buildings;
    }
    function get_model(energies_for_building, temperatures){
        n   = 0;
        Sx  = 0;
        Sy  = 0;
        Sx2 = 0;
        Sxy = 0;
        for(energy_for_day of energies_for_building){
            energy_for_day.temperature = get_temperature_for(energy_for_day.when, temperatures);
            n = n + 1;
            Sx = Sx + energy_for_day.temperature;
            Sy = Sy + energy_for_day.measurement;
            Sx2 = Sx2 + Math.pow(energy_for_day.temperature, 2);
            Sxy = Sxy + energy_for_day.temperature*energy_for_day.measurement;
        };
        var m = (n*Sxy - Sx*Sy)/(n*Sx2 - Math.pow(Sx,2));
        var b = (Sy - m*Sx)/n;
        return({'b': b, 'm':m, energies_for_building: energies_for_building});
    }
    function get_temperature_for(when, temperatures){
        for(temperature of temperatures){
            if(temperature.when === when){
                return temperature.temperature;
            }
        }
    }
    function doh(err){
        console.log("doh err: " + err);
    }
    sql.on('sql error', err => {
        console.log("sql err: " + err);
    })
}

var locations = {
    'rrh': { 'lat': 30.2818296, 'lng': -97.7411531 },
    'aca': { 'lat': 30.2877, 'lng': -97.7357 },
    'adh': { 'lat': 30.2915, 'lng': -97.7406 },
    'afp': { 'lat': 30.2853, 'lng': -97.7263 },
    'ahg': { 'lat': 30.2885, 'lng': -97.7377 },
    'anb': { 'lat': 30.2783, 'lng': -97.731 },
    'and': { 'lat': 30.2882, 'lng': -97.7398 },
    'art': { 'lat': 30.2862, 'lng': -97.733 },
    'att': { 'lat': 30.282, 'lng': -97.7405 },
    'bat': { 'lat': 30.2848, 'lng': -97.7389 },
    'bel': { 'lat': 30.2837, 'lng': -97.7337 },
    'ben': { 'lat': 30.284, 'lng': -97.739 },
    'bgh': { 'lat': 30.2869, 'lng': -97.7388 },
    'bhd': { 'lat': 30.2831, 'lng': -97.736 },
    'bio': { 'lat': 30.2872, 'lng': -97.7398 },
    'bld': { 'lat': 30.2887, 'lng': -97.7394 },
    'bma': { 'lat': 30.281, 'lng': -97.7374 },
    'bmc': { 'lat': 30.2902, 'lng': -97.7408 },
    'bme': { 'lat': 30.2892, 'lng': -97.7386 },
    'bot': { 'lat': 30.287, 'lng': -97.74 },
    'brb': { 'lat': 30.2852, 'lng': -97.7367 },
    'brg': { 'lat': 30.2809, 'lng': -97.7362 },
    'bsb': { 'lat': 30.2814, 'lng': -97.7354 },
    'btl': { 'lat': 30.2854, 'lng': -97.7403 },
    'bur': { 'lat': 30.2888, 'lng': -97.7385 },
    'bwy': { 'lat': 30.2908, 'lng': -97.7382 },
    'cal': { 'lat': 30.2845, 'lng': -97.7402 },
    'cba': { 'lat': 30.2842, 'lng': -97.7378 },
    'ccg': { 'lat': 30.2821, 'lng': -97.7404 },
    'ccj': { 'lat': 30.2881, 'lng': -97.7304 },
    'cda': { 'lat': 30.283, 'lng': -97.7257 },
    'cdl': { 'lat': 30.2788, 'lng': -97.733 },
    'cee': { 'lat': 30.2906, 'lng': -97.7364 },
    'cla': { 'lat': 30.2849, 'lng': -97.7354 },
    'clk': { 'lat': 30.2817, 'lng': -97.7349 },
    'cma': { 'lat': 30.2894, 'lng': -97.7407 },
    'cmb': { 'lat': 30.2892, 'lng': -97.7411 },
    'cml': { 'lat': 30.2829, 'lng': -97.7253 },
    'cpe': { 'lat': 30.2903, 'lng': -97.7361 },
    'crd': { 'lat': 30.2887, 'lng': -97.7401 },
    'crh': { 'lat': 30.2885, 'lng': -97.7334 },
    'cs3': { 'lat': 30.2807, 'lng': -97.7355 },
    'cs4': { 'lat': 30.2887, 'lng': -97.7325 },
    'cs5': { 'lat': 30.2907, 'lng': -97.7355 },
    'cs6': { 'lat': 30.2864, 'lng': -97.7358 },
    'csa': { 'lat': 30.2885, 'lng': -97.7357 },
    'ct1': { 'lat': 30.2866, 'lng': -97.7348 },
    'dcp': { 'lat': 30.2762, 'lng': -97.733 },
    'dev': { 'lat': 30.2873, 'lng': -97.7236 },
    'dfa': { 'lat': 30.2859, 'lng': -97.7318 },
    'dff': { 'lat': 30.2794, 'lng': -97.7265 },
    'dtb': { 'lat': 30.2873, 'lng': -97.7322 },
    'eas': { 'lat': 30.2811, 'lng': -97.7382 },
    'ecj': { 'lat': 30.289, 'lng': -97.7354 },
    'eer': { 'lat': 30.288012, 'lng': -97.73515 },
    'eme': { 'lat': 30.389668, 'lng': -97.727699 },
    'ens': { 'lat': 30.2881, 'lng': -97.7353 },
    'eps': { 'lat': 30.2858, 'lng': -97.7367 },
    'erc': { 'lat': 30.2769, 'lng': -97.7322 },
    'etc': { 'lat': 30.2899, 'lng': -97.7354 },
    'fac': { 'lat': 30.2863, 'lng': -97.7404 },
    'fc1': { 'lat': 30.2846, 'lng': -97.7226 },
    'fc2': { 'lat': 30.2839, 'lng': -97.7231 },
    'fc3': { 'lat': 30.2848, 'lng': -97.7237 },
    'fc4': { 'lat': 30.2843, 'lng': -97.7236 },
    'fc5': { 'lat': 30.2836, 'lng': -97.7246 },
    'fc6': { 'lat': 30.2834, 'lng': -97.7258 },
    'fc7': { 'lat': 30.2833, 'lng': -97.7264 },
    'fc8': { 'lat': 30.2854, 'lng': -97.7239 },
    'fc9': { 'lat': 30.2838, 'lng': -97.724 },
    'fct': { 'lat': 30.2843, 'lng': -97.7224 },
    'fdh': { 'lat': 30.2894, 'lng': -97.7325 },
    'fnt': { 'lat': 30.2879, 'lng': -97.7379 },
    'fpc': { 'lat': 30.2794, 'lng': -97.725 },
    'gar': { 'lat': 30.2852, 'lng': -97.7385 },
    'gdc': { 'lat': 30.2863, 'lng': -97.7365 },
    'gea': { 'lat': 30.2877, 'lng': -97.7392 },
    'geb': { 'lat': 30.2863, 'lng': -97.7386 },
    'gol': { 'lat': 30.2854, 'lng': -97.7412 },
    'grc': { 'lat': 30.2838, 'lng': -97.7359 },
    'gre': { 'lat': 30.284, 'lng': -97.7364 },
    'grf': { 'lat': 30.2842, 'lng': -97.7356 },
    'grg': { 'lat': 30.2877, 'lng': -97.7399 },
    'grp': { 'lat': 30.2844, 'lng': -97.7359 },
    'grs': { 'lat': 30.2836, 'lng': -97.736 },
    'gsb': { 'lat': 30.2842, 'lng': -97.7383 },
    'gug': { 'lat': 30.2795, 'lng': -97.7432 },
    'hdb': { 'lat': 30.27775, 'lng': -97.7348 },
    'hlb': { 'lat': 30.2754, 'lng': -97.73346 },
    'hma': { 'lat': 30.2869, 'lng': -97.7406 },
    'hrc': { 'lat': 30.2843, 'lng': -97.7412 },
    'hrh': { 'lat': 30.2842, 'lng': -97.7402 },
    'hsm': { 'lat': 30.2889, 'lng': -97.7408 },
    'htb': { 'lat': 30.27742, 'lng': -97.73511 },
    'icb': { 'lat': 30.3148, 'lng': -97.7278 },
    'int': { 'lat': 30.2884, 'lng': -97.7434 },
    'ipf': { 'lat': 30.2863, 'lng': -97.7265 },
    'jcd': { 'lat': 30.2822, 'lng': -97.7363 },
    'jes': { 'lat': 30.2829, 'lng': -97.7368 },
    'jgb': { 'lat': 30.2859, 'lng': -97.7357 },
    'jhh': { 'lat': 30.2784, 'lng': -97.732 },
    'jjf': { 'lat': 30.2835, 'lng': -97.7325 },
    'jon': { 'lat': 30.2886, 'lng': -97.7317 },
    'kin': { 'lat': 30.2904, 'lng': -97.7396 },
    'lbj': { 'lat': 30.2858, 'lng': -97.7292 },
    'lch': { 'lat': 30.2886, 'lng': -97.7408 },
    'ldh': { 'lat': 30.2826, 'lng': -97.7359 },
    'lfh': { 'lat': 30.2881, 'lng': -97.7408 },
    'lla': { 'lat': 30.2906, 'lng': -97.7405 },
    'llb': { 'lat': 30.2909, 'lng': -97.7405 },
    'llc': { 'lat': 30.2911, 'lng': -97.7405 },
    'lld': { 'lat': 30.2906, 'lng': -97.7409 },
    'lle': { 'lat': 30.2909, 'lng': -97.741 },
    'llf': { 'lat': 30.2911, 'lng': -97.7408 },
    'ltd': { 'lat': 30.2893, 'lng': -97.7397 },
    'lth': { 'lat': 30.286, 'lng': -97.7352 },
    'mag': { 'lat': 30.2828, 'lng': -97.7309 },
    'mai': { 'lat': 30.286, 'lng': -97.7394 },
    'mbb': { 'lat': 30.2885, 'lng': -97.7373 },
    'mez': { 'lat': 30.2844, 'lng': -97.7389 },
    'mfh': { 'lat': 30.282, 'lng': -97.7311 },
    'mhd': { 'lat': 30.2836, 'lng': -97.7353 },
    'mms': { 'lat': 30.2826, 'lng': -97.7301 },
    'mnc': { 'lat': 30.2823, 'lng': -97.7327 },
    'mrh': { 'lat': 30.2873, 'lng': -97.7309 },
    'msb': { 'lat': 30.2827, 'lng': -97.7257 },
    'nez': { 'lat': 30.2846, 'lng': -97.7325 },
    'nhb': { 'lat': 30.2876, 'lng': -97.738 },
    'nms': { 'lat': 30.2892, 'lng': -97.7376 },
    'noa': { 'lat': 30.2911, 'lng': -97.7375 },
    'nur': { 'lat': 30.2777, 'lng': -97.7336 },
    'pac': { 'lat': 30.2864, 'lng': -97.7311 },
    'pai': { 'lat': 30.287, 'lng': -97.7387 },
    'par': { 'lat': 30.2848, 'lng': -97.7402 },
    'pat': { 'lat': 30.288, 'lng': -97.7364 },
    'pcl': { 'lat': 30.2828, 'lng': -97.7382 },
    'phd': { 'lat': 30.2824, 'lng': -97.7351 },
    'phr': { 'lat': 30.2882, 'lng': -97.7386 },
    'pob': { 'lat': 30.2869, 'lng': -97.7366 },
    'ppa': { 'lat': 30.2869, 'lng': -97.7344 },
    'ppb': { 'lat': 30.2813, 'lng': -97.7269 },
    'ppe': { 'lat': 30.2869, 'lng': -97.7359 },
    'ppl': { 'lat': 30.2865, 'lng': -97.7352 },
    'rhd': { 'lat': 30.2831, 'lng': -97.7351 },
    'rlm': { 'lat': 30.2889, 'lng': -97.7363 },
    'rsc': { 'lat': 30.2815, 'lng': -97.7324 },
    'sac': { 'lat': 30.2849, 'lng': -97.7363 },
    'sag': { 'lat': 30.2887, 'lng': -97.7427 },
    'sbs': { 'lat': 30.2805, 'lng': -97.725 },
    'sea': { 'lat': 30.29, 'lng': -97.7373 },
    'ser': { 'lat': 30.2877, 'lng': -97.7346 },
    'sjg': { 'lat': 30.2877, 'lng': -97.7329 },
    'sjh': { 'lat': 30.2823, 'lng': -97.7344 },
    'sof': { 'lat': 30.2809, 'lng': -97.7275 },
    'srh': { 'lat': 30.285, 'lng': -97.7289 },
    'ssb': { 'lat': 30.2901, 'lng': -97.7384 },
    'ssw': { 'lat': 30.2806, 'lng': -97.7327 },
    'std': { 'lat': 30.2836, 'lng': -97.7323 },
    'sut': { 'lat': 30.285, 'lng': -97.7408 },
    'sw7': { 'lat': 30.2908, 'lng': -97.7363 }, 
    'swg': { 'lat': 30.2912, 'lng': -97.7371 },
    'szb': { 'lat': 30.2817, 'lng': -97.7388 },
    'tcc': { 'lat': 30.287, 'lng': -97.729 },
    'tmm': { 'lat': 30.287, 'lng': -97.7324 },
    'tnh': { 'lat': 30.2886, 'lng': -97.7307 },
    'trg': { 'lat': 30.2791, 'lng': -97.7339 },
    'tsb': { 'lat': 30.3152, 'lng': -97.7267 },
    'tsc': { 'lat': 30.2799, 'lng': -97.7335 },
    'tsg': { 'lat': 30.2913, 'lng': -97.7386 },
    'ttc': { 'lat': 30.2778, 'lng': -97.7348 },
    'ua9': { 'lat': 30.2903, 'lng': -97.7387 },
    'uil': { 'lat': 30.2833, 'lng': -97.7236 },
    'unb': { 'lat': 30.2866, 'lng': -97.7411 },
    'upb': { 'lat': 30.2841, 'lng': -97.7304 },
    'uss': { 'lat': 30.2926, 'lng': -97.7363 },
    'uta': { 'lat': 30.2793, 'lng': -97.7428 },
    'utc': { 'lat': 30.2831, 'lng': -97.7388 },
    'utx': { 'lat': 30.2843, 'lng': -97.7344 },
    'wag': { 'lat': 30.2851, 'lng': -97.7376 },
    'wat': { 'lat': 30.2784, 'lng': -97.7336 },
    'wch': { 'lat': 30.2861, 'lng': -97.7384 },
    'wel': { 'lat': 30.2866, 'lng': -97.7377 },
    'win': { 'lat': 30.2859, 'lng': -97.7345 },
    'wmb': { 'lat': 30.2854, 'lng': -97.7406 },
    'wrw': { 'lat': 30.2875, 'lng': -97.7359 },
    'wwh': { 'lat': 30.2893, 'lng': -97.7418 },
};

var full_names = {
    rrh: 'ROBERT b. ROWLING HALL',
    adh: 'ALMETRIS DUREN RESIDENCE HALL',
    ahg: 'ANNA HISS GYMNASIUM',
    anb: 'ARNO NOWOTNY BUILDING',
    and: 'ANDREWS DORMITORY',
    arc: 'ANIMAL RESOURCES CENTER',
    art: 'ART BUILDING AND MUSEUM',
    att: 'AT&T EXECUTIVE EDUC & CONF CENTER',
    bat: 'BATTS HALL',
    bel: 'L. THEO BELLMONT HALL',
    ben: 'BENEDICT HALL',
    bhd: 'BRACKENRIDGE HALL DORM',
    bio: 'BIOLOGICAL LABORATORIES',
    bld: 'BLANTON DORMITORY',
    bma: 'JACK S. BLANTON MUSEUM OF ART',
    bmc: 'BELO CENTER FOR NEW MEDIA',
    bme: 'BIOMEDICAL ENGINEERING BUILDING',
    brb: 'BERNARD AND AUDRE RAPOPORT BUILDING',
    brg: 'BRAZOS GARAGE',
    btl: 'BATTLE HALL',
    bur: 'BURDINE HALL',
    bwy: 'BWY',
    cal: 'CALHOUN HALL',
    cba: 'COLLEGE OF BUSINESS ADMINISTRATION',
    ccj: 'CONNALLY CENTER FOR JUSTICE',
    cdl: 'COLLECTIONS DEPOSIT LIBRARY',
    cla: 'LIBERAL ARTS BUILDING',
    cma: 'JESSE H. JONES COMM. CTR. (BLDG. A)',
    cmb: 'JESSE H. JONES COMM. CTR. (BLDG. B)',
    com: 'COMPUTATION CENTER',
    cp1: 'CHILLING PLANT #1',
    cpe: 'CHEMICAL AND PETROLEUM ENGINEERING',
    crb: 'COMPUTATIONAL RESOURCE BUILDING',
    crd: 'CAROTHERS DORMITORY',
    crh: 'CREEKSIDE RESIDENCE HALL',
    cs3: 'CENTRAL CHILLING STATION NO. 3',
    cs4: 'CENTRAL CHILLING STATION NO. 4',
    cs5: 'CENTRAL CHILLING STATION NO. 5',
    cs6: 'CENTRAL CHILLING STATION NO. 6',
    cs7: 'CENTRAL CHILLING STATION NO. 7',
    dcp: 'DENTON A. COOLEY PAVILION',
    dfa: 'E. WILLIAM DOTY FINE ARTS BUILDING',
    dff: 'UFCU DISCH-FALK FIELD',
    eas: 'EDGAR A. SMITH BUILDING',
    ecj: 'ERNEST COCKRELL JR. HALL',
    eme: 'J. J. Pickle Research',
    eps: 'E.P. SCHOCH BUILDING',
    erc: 'FRANK C ERWIN SPECIAL EVENTS CENTER',
    etc: 'ENGINEERING TEACHING CENTER II',
    fac: 'PETER T. FLAWN ACADEMIC CENTER',
    gar: 'GARRISON HALL',
    gdc: 'GATES DELL COMPLEX',
    gea: 'MARY E. GEARING HALL',
    geb: 'DOROTHY L. GEBAUER BUILDING',
    gol: 'GOLDSMITH HALL',
    gre: 'GREGORY GYMNASIUM',
    grg: 'GEOGRAPHY BUILDING',
    gsb: 'GRADUATE SCHOOL OF BUSINESS BLDG.',
    hdb: 'HEALTH DISCOVERY BUILDING',
    hgc: 'NULL',
    hlb: 'HEALTH LEARNING BUILDING',
    hma: 'HOGG MEMORIAL AUDITORIUM',
    hrc: 'HARRY RANSOM CENTER',
    hrh: 'RAINEY HALL',
    hsm: 'WILLIAM RANDOLPH HEARST BLDG',
    htb: 'HEALTH TRANSFORMATION BUILDING',
    jcd: 'JESTER DORMITORY',
    jes: 'BEAUFORD H. JESTER CENTER',
    jgb: 'JACKSON GEOLOGICAL SCIENCES BLDG.',
    jhh: 'JOHN W. HARGIS HALL',
    jon: 'JESSE H. JONES HALL',
    kin: 'KINSOLVING DORMITORY',
    lbj: 'LYNDON B JOHNSON LIBRARY',
    ldh: 'LONGHORN DINING FACILITY',
    lfh: 'LITTLEFIELD HOME',
    lla: 'LIVING LEARNING HALL A',
    llb: 'LIVING LEARNING HALL B',
    llc: 'LIVING LEARNING HALL C',
    lld: 'LIVING LEARNING HALL D',
    lle: 'LIVING LEARNING HALL E',
    llf: 'LIVING LEARNING HALL F',
    ltd: 'LITTLEFIELD DORMITORY',
    lth: 'LABORATORY THEATER BLDG.',
    mag: 'MANOR GARAGE',
    mai: 'MAIN BUILDING',
    mbb: 'MOFFETT MOLECULAR BIOLOGY BLDG.',
    mez: 'MEZES HALL',
    mfh: 'RICHARD MITHOFF TRK/SCR  FIELDHOUSE',
    mhd: 'MOORE-HILL DORMITORY',
    mms: 'MIKE A.MYERS TRACK & SOCCER STADIUM',
    mnc: 'MONCRIEF-NEUHAUS ATHLETIC CENTER',
    mrh: 'MUSIC BUILDING & RECITAL HALL',
    msb: 'MAIL SERVICE BUILDING',
    nez: 'NORTH END ZONE BUILDING',
    nhb: 'NORMAN HACKERMAN BUILDING',
    nms: 'NEURAL AND MOLECULAR SCIENCE BLDG.',
    noa: 'NORTH OFFICE BUILDING A',
    nst: 'NST',
    nur: 'NURSING SCHOOL',
    pac: 'PERFORMING ARTS CENTER',
    pai: 'T.S. PAINTER HALL',
    par: 'PARLIN HALL',
    pat: 'J.T. PATTERSON LABS.BLDG.',
    pcl: 'PERRY-CASTANEDA LIBRARY',
    phd: 'PRATHER HALL DORMITORY',
    phr: 'PHARMACY BUILDING',
    pob: 'PETER ODONNELL JR. BUILDING',
    ppa: 'HAL C. WEAVER POWER PLANT ANNEX',
    ppb: 'PRINTING AND PRESS BLDG.',
    ppe: 'HAL C WEAVER POWER PLANT EXPANSION',
    ppl: 'HAL C. WEAVER POWER PLANT',
    rhd: 'ROBERTS HALL DORMITORY',
    rlm: 'ROBERT LEE MOORE HALL',
    rsc: 'RECREATIONAL SPORTS CENTER',
    sac: 'STUDENT ACTIVITY CENTER',
    sea: 'SARAH M. & CHARLES E. SEAY BUILDING',
    ser: 'SERVICE BUILDING',
    sjg: 'SAN JACINTO GARAGE',
    sjh: 'SAN JACINTO RESIDENCE HALL',
    smc: 'SETON MEDICAL CENTER',
    sof: 'TELECOMM.SVC.SATELLITE OPS FACILITY',
    srh: 'SID RICHARDSON HALL',
    ssb: 'STUDENT SERVICES BUILDING',
    ssw: 'SCHOOL OF SOCIAL WORK BUILDING',
    std: 'DARRELL K ROYAL TX MEMORIAL STADIUM',
    sut: 'SUTTON HALL',
    sw7: 'SPEEDWAY OFFICE BUILDING',
    swg: 'SPEEDWAY GARAGE',
    szb: 'GEORGE I. SANCHEZ BUILDING',
    tcc: 'JOE C THOMPSON CONFERENCE CENTER',
    tes1: 'THERMAL ENERGY STORAGE 1',
    tes2: 'THERMAL EXPANSION TANK 2',
    tmm: 'TEXAS MEMORIAL MUSEUM',
    tnh: 'TOWNES HALL',
    trg: 'TRINITY GARAGE',
    tsc: 'LEE & JOE JAMAIL TEXAS SWIMMING CTR',
    tsg: '27TH STREET GARAGE',
    ua9: '2609 UNIVERSITY AVENUE',
    uil: 'UNIV. INTERSCHOLASTIC LEAGUE BLDG.',
    unb: 'UNION BUILDING',
    upb: 'UNIVERSITY POLICE BUILDING',
    uta: 'UT ADMINISTRATION BUILDING',
    utc: 'UNIVERSITY TEACHING CENTER',
    utx: 'ETTER-HARBIN ALUMNI CENTER',
    wag: 'WAGGENER HALL',
    wch: 'WILL C. HOGG BLDG.',
    wel: 'ROBERT A. WELCH HALL',
    win: 'F.L. WINSHIP DRAMA BLDG.',
    wmb: 'WEST MALL OFFICE BLDG.',
    wrw: 'W.R. WOOLRICH LABS.',
    wwh: 'WALTER WEBB HALL' 
};

function get_unacknowledged_cleared_alarms(tags) {
    var tags_out = [];
    for(tag of tags){
        if(tag.alarming || !tag.acknowledged){
            tags_out.push(tag);
        }
    }
    debug("get_unacknowledged_cleared_alarms: " + tags.length);
    return tags_out; 
}
exports.login = function(credentials) { 
    console.log("credentials stored: " + JSON.stringify(credentials));
    user_name = credentials.user_name;
    password = credentials.password;
};
exports.alarms_for_management_map = function(callback) {
    var ignition_alarms_uri  = "http://" + user_name + ":" + password + "@" + ignition_machine+":"+ignition_port+ignition_alarms_path; 
    var body = "";
    http.get(ignition_alarms_uri, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        try {
            var response = JSON.parse(body);
            var ent_opc_connection_state = response.ent_opc_connection_state;
            var tags = response.alarms;
            tags = get_latest_alarms(tags);
            tags = eliminate_duplicate_buildings(tags);
            tags = set_building_plc_meter_acquisition(tags);
            tags = add_full_names(tags);
            tags = get_utility_type(tags);
            tags = display_tag(tags);
            tags = is_multiple_alarms_for_building(tags);
            callback(tags); 
        } catch (err){
            console.log('exception2: ' + err);
            callback([]); 
        }
      });
    });
    function eliminate_duplicate_buildings(tags){
        var buildings = [];
        var unique_alarms = [];
        for(i=0; i!=tags.length; i++){
            if(buildings.indexOf(tags[i].who) == -1){
                buildings.push(tags[i].who);
                unique_alarms.push(tags[i]);
            }
        }
        return unique_alarms;
    };
};
function set_building_plc_meter_acquisition(tag_objects){
    for(tag_object of tag_objects){
        // console.log(tag_object);
        var tag = tag_object.who.split(":/")[1].split(":")[1];
        var building = tag.substring(0,3).toLowerCase();
        tag_object.building = building;
        tag_object.where = locations[building];
        tag_object.plc = tag.split("/")[1];
        var count_of_slashes = tag.split("/").length - 1;
        switch(count_of_slashes) {
            case 2:
                var count_of_underscores = tag.split("/")[2].split("_").length - 1;
                switch(count_of_underscores){
                    case 1:
                        tag_object.meter = tag.split("/")[2].split("_")[0];
                        tag_object.acquisition_type = tag.split("/")[2].split("_")[1];
                        break
                    case 2:
                        tag_object.meter = tag.split("/")[2].split("_")[1];
                        tag_object.acquisition_type = tag.split("/")[2].split("_")[2];
                        break
                    case 3:
                        tag_object.meter = tag.split("/")[2].split("_").slice(1,3).join("_");
                        tag_object.acquisition_type = tag.split("/")[2].split("_")[3];
                        break
                    default:
                        break;
                }
                break;
            case 3:
                tag_object.meter = tag.split("/")[2];
                tag_object.acquisition_type = tag.split("/")[3];
                break;
            default:
                break;
        }   
        // console.log(tag_object);
    }
    return tag_objects;
};

function should_siren(tags){
    for(tag of tags){
        var alarming = tag.alarming;
        var acknowledged = tag.acknowledged;
        var escalated = tag.escalated;
        tag.should_siren = (!alarming && escalated) || (alarming && !acknowledged);
    }
    debug("should_siren: " + tags.length);
    return tags;
}

function sort_column(tag_objects, column_to_sort, sort_ascending){
    try{
        if(typeof sort_ascending != 'undefined'){
            // console.log("sort_column " + column_to_sort + " by " + sort_ascending);
            column_to_sort = column_to_sort.toLowerCase();
            tag_objects.sort(function(a, b){
                // console.log(column_to_sort);
                var left_value, right_value, result;
                switch(column_to_sort.toLowerCase()) {
                  case 'time':
                    // console.log("  time");
                    left_value = a.active_data.time;
                    right_value = b.active_data.time;
                    if (left_value <  right_value) result = -1;  // any negative number works
                    if (left_value >  right_value) result =  1;   // any positive number works
                    if (left_value == right_value) result =  0; // equal values MUST yield zero
                    break;
                  case 'condition':
                    // console.log("  condition");
                    left_value  = a.who.split(":/")[2].split(":")[1] + " " + a.active_data.mode + " " + a.active_data.setpointA;
                    right_value  = b.who.split(":/")[2].split(":")[1] + " " + b.active_data.mode + " " + b.active_data.setpointA;
                    if (left_value <  right_value) result = -1;  // any negative number works
                    if (left_value >  right_value) result =  1;   // any positive number works
                    if (left_value == right_value) result =  0; // equal values MUST yield zero
                    break;
                  case 'value':
                    // console.log("  value");
                    try{
                      left_value = parseInt(JSON.parse(a.active_data.eventValue));
                    } catch (error) {
                      left_value = JSON.parse(a.active_data.eventValue.toLowerCase());
                    }
                    try{
                      right_value = parseInt(JSON.parse(b.active_data.eventValue));
                    } catch (error) {
                      right_value = JSON.parse(b.active_data.eventValue.toLowerCase());
                    }
                    if (left_value <  right_value) result = -1;  // any negative number works
                    if (left_value >  right_value) result =  1;   // any positive number works
                    if (left_value == right_value) result =  0; // equal values MUST yield zero
                    break;
                  case 'alarming':
                    // console.log("  alarming");
                    var state;

                    if(a.alarming){
                        if(!a.acknowledged){
                            state = 0;
                        } else {
                            state = 1;
                        }
                    } else {
                        if(!a.acknowledged){
                            state = 3;
                        }
                    }
                    left_value = state;

                    if(b.alarming){
                        if(!b.acknowledged){
                            state = 0;
                        } else {
                            state = 1;
                        }
                    } else {
                        if(!b.acknowledged){
                            state = 3;
                        }
                    }
                    right_value = state;
                    
                    if (left_value <  right_value) result = -1;  // any negative number works
                    if (left_value >  right_value) result =  1;   // any positive number works
                    if (left_value == right_value) result =  0; // equal values MUST yield zero
                    break;
                  default:
                    // console.log("  default");
                    left_value = a[column_to_sort];
                    right_value = b[column_to_sort];
                    if (left_value <  right_value) result = -1;  // any negative number works
                    if (left_value >  right_value) result =  1;   // any positive number works
                    if (left_value == right_value) result =  0; // equal values MUST yield zero
                }
                // console.log("    result: " + result);
                // console.log("    left_value:  "  + left_value);
                // console.log("    right_value: " + right_value);
                return result;
            });
            if(!sort_ascending){
                tag_objects.reverse();
            }
        }
        return tag_objects;
    } catch(err) {
        console.log("sort error");

    }
}

exports.alarms_for_management_table = function(column_to_sort, sort_ascending, callback) { 
    console.log("credentials sent to IA server --> " + user_name + " : " + password);
    var ignition_alarms_uri  = "http://" + user_name + ":" + password + "@" + ignition_machine+":"+ignition_port+ignition_alarms_path; 
    // ignition_alarms_uri = "http://adrama:letmein@127.0.0.1:8088/system/webdev/IAMeters/alarms";
    console.log(ignition_alarms_uri);
    var body = "";
    http.get(ignition_alarms_uri, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => { 
        if((body.toLowerCase().indexOf("malformed") == -1) && (body.toLowerCase().indexOf("denied") == -1)){
            try {
                console.log(body);
                var response = JSON.parse(body);
                var ent_opc_connection_state = response.ent_opc_connection_state;
                var tag_objects = response.alarms;
                debug("-----------------------: " + tag_objects.length);
                tag_objects = get_latest_alarms(tag_objects);
                tag_objects = get_utility_type(tag_objects);
                tag_objects = display_tag(tag_objects);
                tag_objects = set_building_plc_meter_acquisition(tag_objects);
                tag_objects = should_siren(tag_objects);
                tag_objects = sort_column(tag_objects, column_to_sort, sort_ascending);
                var to_data_service = {};
                to_data_service.ia_opc_server_state = "CONNECTED";
                to_data_service.ent_opc_connection_state = ent_opc_connection_state;
                to_data_service.tags = tag_objects;
                callback(to_data_service);
            } catch(err) { 
                debugger;
                var to_data_service = {};
                to_data_service.ia_opc_server_state = "FAULTED";
                to_data_service.ent_opc_connection_state = "UNKNOWN";
                to_data_service.tags = [];
                callback(to_data_service);
            }
        } else {
            callback("malformed authorization request");
        }
      });
    })
    .on("error", function (){
        callback("No response from IA Server")
    });
};

exports.alarms_for_history = function(callback) { 
    var url = ignition_history_uri; 
    var body = "";
    http.get(url, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        try {
            var data = JSON.parse(body);
            var tags = data.rows;
            tags = exports.normalize_alarms_historical(tags);
            tags = filter_for_building_in_alarm(tags);
            tags = sort_alarms_for_history(tags);
            tags = presentation_logic(tags);
            callback(tags);
        } catch (err){
            console.log('exception1a: ' + err);
            callback([]); 
        }
      });
    });
    function filter_for_building_in_alarm(tags){
        var tags_filtered = []; 
        for(tag of tags){
            if(tag.who.indexOf('/') == -1){
                tags_filtered.push(tag);
            } else {
                var tag_path = tag.who.split(':')[3];
                var tag_name = tag_path.split('/')[1];
                var building_tag = tag_name.toLowerCase() != 'syserr'.toLowerCase();
                if(building_tag)
                    tags_filtered.push(tag);
            }
        };
        return tags_filtered;
    }
    function presentation_logic(tags){
        var tags_formatted = [];
        for(tag of tags){
            if(tag.who.indexOf('/') == -1){
                tags_formatted.push(tag);
            } else {
                var pieces = tag.who.split(':');
                var building = pieces[3].split('/')[0];
                var building_tag = pieces[3].split('/')[1];
                tag.who = building.toUpperCase()+'-'+building_tag;
                tags_formatted.push(tag);
            }
        }
        return tags_formatted;
    }
};

exports.tag_history = function(tag_needing_history, callback) { 
    console.log("tag_needing_history: " + tag_needing_history);
    var url = tag_history_uri + "?tag_needing_history=" + tag_needing_history; 
    var body = "";
    console.log(url);
    http.get(url, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        try {
            var data = JSON.parse(body);
            var tags = data.rows;
            console.log(tags.length);
            tags = exports.normalize_alarms_historical(tags);
            console.log(tags.length);
            tags = filter_out_events(tags);
            console.log(tags.length);
            tags = sort_alarms_for_history(tags);
            console.log(tags.length);
            callback(tags);
        } catch (err){
            console.log('exception1b: ' + err);
            callback([]); 
        }
      });
    });
    function filter_out_events(tags){
        var tags_filtered = []; 
        for(tag of tags){
            if(tag.who.indexOf('/') == -1){
                tags_filtered.push(tag);
            } else {
                var tag_path = tag.who.split(':')[3];
                var tag_name = tag_path.split('/')[1];
                var building_tag = tag_name.toLowerCase() != 'syserr'.toLowerCase();
                if(building_tag)
                    tags_filtered.push(tag);
            }
        };
        return tags_filtered;
    }
};

exports.alarms_acknowledge = function(data, callback) { 
    var body = "";
    var post_data = querystring.stringify(data);
    var post_options = {
      auth: user_name+":"+password,
      host: ignition_machine,
      port: ignition_port,
      path: ignition_alarms_path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data),
          'Access-Control-Allow-Origin': '*'
      }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on("end", () => {
            callback(JSON.stringify(body)); 
        });
    });
    post_req.write(post_data);
    post_req.end();
};
function filter_for_not_pd(tags) {
    var return_these = [];
    tags.forEach(function(tag){
        var found_pd = tag.who.toLowerCase().indexOf("_pd") != -1;
        if(!found_pd){
            return_these.push(tag);
        }
    })
    debug("filter_for_not_pd: " + return_these.length);
    return return_these; 
}
function get_latest_alarms(tags) {
    var alarms_latest_who = [];
    var alarms_latest_alarms = [];
    for(tag of tags) {
        var found_at = alarms_latest_who.indexOf(tag.who);
        if(found_at != -1){
            if(alarms_latest_alarms[found_at].active_data.time < tag.active_data.time){
                alarms_latest_alarms[found_at] = tag;
            }
        } else {
            alarms_latest_who.push(tag.who);
            alarms_latest_alarms.push(tag);
        }
    }
    debug("get_latest_alarms: " + alarms_latest_alarms.length);
    return alarms_latest_alarms; 
}
function get_utility_type(tags) {
    var returned_tags = [];
    tags.forEach(function(tag){
        var tag_path_pieces = tag.who.toLowerCase().split(":/")[1].split(":")[1].split("/");
        var first_folder = tag_path_pieces[0];
        var second_folder = tag_path_pieces[1];
        var meter = tag_path_pieces[2];
        if (typeof meter == 'undefined'){
            meter = 'undefined';
        }
        var meter_pieces = meter.split("_");
        var number_meter_pieces = meter_pieces.length;
        switch(number_meter_pieces){
            case 1:
                var utility_type = "no underscores";
                break;
            case 2:
                var utility_type = "unknown";
                break;
            case 3:
                var utility_type = meter_pieces[1][0];
                break;
            default:
                var utility_type = "unknown";
                break;
        }
        switch(utility_type){
            case "no underscores":
                tag["utility_type"] = "plc";
                break;
            case "a":
                tag["utility_type"] = "chilled water";
                break;
            case "e":
                tag["utility_type"] = "electricity";
                break;
            case "s":
                tag["utility_type"] = "steam";
                break
            case "w":
                tag["utility_type"] = "water";
                break;
            default:
                tag["utility_type"] = "unknown";
                break;
        }
        returned_tags.push(tag);
    })
    debug("get_utility_type: " + returned_tags.length);
    return returned_tags; 
}
function display_tag(tags) {
    var returned_tags = [];
    tags.forEach(function(tag){
        if(!tag.acknowledged || tag.alarming){
            returned_tags.push(tag);
        }
    })
    debug("display_tag: " + returned_tags.length);
    return returned_tags; 
}
function get_building(tag){
    return tag.who.split(":/")[1].split(":")[1].split("/")[0];
}
function set_buildings(tags){
    var return_tags = [];
    tags.forEach(function(tag){
        var building = tag.who.split(":/")[1].split(":")[1].split("/")[0].toLowerCase();
        if(building.indexOf("_") != -1){
            building = building.split("_")[0];
        }
        tag["building"] = building;
        tag["where"] = locations[building];
        return_tags.push(tag);
    })
    debug("set_buildings: " + return_tags.length);
    return return_tags; 
}
function set_plcs(tags){
    var return_tags = [];
    tags.forEach(function(tag){
        tag["plc"] = tag.who.split(":/")[1].split(":")[1].split("/")[1];
        return_tags.push(tag);
    })
    debug("set_plcs: " + return_tags.length);
    return return_tags; 
}
function set_meters(tags){
    var return_tags = [];
    tags.forEach(function(tag){
        tag["meter"] = tag.who.split(":/")[1].split(":")[1].split("/")[2];
        return_tags.push(tag);
    })
    debug("set_meters: " + return_tags.length);
    return return_tags; 
}
function is_multiple_alarms_for_building(tags) {
    var buildings_with_alarms = [];
    tags.forEach(function(tag){
        if(tag.alarming){
            var building = get_building(tag);
            buildings_with_alarms.push(building);
        }
    })
    tags.forEach(function(tag){
        var building = get_building(tag);
        if(buildings_with_alarms.filter(function(building_in_alarm){return building_in_alarm == building}).length == 1){
            tag["multiple_alarms"] = false;
        } else {
            tag["multiple_alarms"] = true;
        }
    })
    debug("is_multiple_alarms_for_building: " + tags.length);
    return tags; 
}
function sort_alarms_for_history(tags) {
    tags.sort(function(a, b){return b.when_alarmed - a.when_alarmed});
    return tags; 
}
normalize_alarm_from_event = function(tag){
    // var who = tag["source"];
    // var uuid = tag[1];
    // var alarming = JSON.parse(tag[2]);
    // var when_alarmed = Date.parse(tag[3]);
    // var why_alarming = tag[4];
    // var acknowledged = JSON.parse(tag[5]); 
    // var ack_notes = tag[6];
    // var when_cleared = Date.parse(tag[7]);

    // alarm_data_normalized = {
    //     "who":who, 
    //     "uuid":uuid, 
    //     "alarming":alarming,  
    //     "when_alarmed":when_alarmed, 
    //     "why_alarming":why_alarming, 
    //     "acknowledged":acknowledged,
    //     "ack_notes":ack_notes,
    //     "when_cleared":when_cleared,
    // }; 
    return alarm_data_normalized; 
}
normalize_alarms_from_events = function(tags) {
    var alarms_normalized = []; 
    for(tag of tags){
        var alarm_normalized = normalize_alarm_from_event(tag);
        alarms_normalized.push(alarm_normalized); 
    }
    debug("normalize_alarms_from_events: " + alarms_normalized.length);
    return alarms_normalized; 
}
exports.normalize_alarm_historical = function(tag){
    var uuid = tag[5];
    var who = tag[0];
    if (who.indexOf('/') == -1){
        // who = '(none)'
    } else {
        var who = tag[0];
        var pieces_on_slash = who.split('/');
        var pieces_on_colon = pieces_on_slash[1].split(':');
        var building_part_uppercased = pieces_on_colon[0] + ':' + pieces_on_colon[1].toUpperCase();
        who = pieces_on_slash[0] + '/' + building_part_uppercased + '/' + pieces_on_slash[2] + '/' + pieces_on_slash[3];
    }
    tag[0] = who;
    var alarmed_data = tag[1];
    var alarming = alarmed_data.indexOf('eventTime') != -1 ? 1 : 0;
    var acknowledged_data = tag[2];
    var acknowledged = acknowledged_data.indexOf('eventTime') != -1 ? 1 : 0;
    var cleared_data = tag[3]; 
    var cleared = cleared_data.indexOf('eventTime') != -1 ? 1 : 0;
    var state = cleared * 4 + acknowledged * 2 + alarming; 
    var because; 
    switch(state){ 
        case 1: // alarming
            alarmed_data = alarmed_data.substring(1,alarmed_data.length-1); 
            alarmed_data = alarmed_data.split(', ');
            var aHash = {};
            for(elementN in alarmed_data){
                var pieces = alarmed_data[elementN].split('=');
                var lpiece = pieces[0];
                var rpiece = pieces[1];
                aHash[lpiece] = rpiece;
            }
            timeAck = aHash['eventTime'];
            because = 'It became ' + aHash['eventValue'];
            break;
        case 2: 
        case 3: // Acknowledged
            acknowledged_data = acknowledged_data.substring(1,acknowledged_data.length-1);
            acknowledged_data = acknowledged_data.split(', ');
            var aHash = {};
            for(elementN in acknowledged_data){
                var pieces = acknowledged_data[elementN].split('=');
                var lpiece = pieces[0];
                var rpiece = pieces[1];
                aHash[lpiece] = rpiece;
            }
            timeAck = aHash['eventTime'];
            user = aHash['ackUser']; 
            if(user != undefined) 
                var user = user.split(':')[1]; 
            if(user == 'Live Event Limit'){
                var reason = "it was automatically acknowledged"
            } else {
                var reason = aHash['ackNotes']
            }
            because = user + ' SAYS ' + reason;
            break;
        case 4: 
        case 5: 
        case 6: 
        case 7: // Cleared
            cleared_data = cleared_data.substring(1,cleared_data.length-1);
            cleared_data = cleared_data.split(', ');
            var aHash = {};
            for(elementN in cleared_data){
                var pieces = cleared_data[elementN].split('=');
                var lpiece = pieces[0];
                var rpiece = pieces[1];
                aHash[lpiece] = rpiece;
            }
            timeAck = aHash['eventTime'];
            because = 'It became ' + aHash['eventValue'];
            break; 
        default:
            timeAck = '(unknown)';
    }; 
    switch(state){
        case 1:
            var became = 'Active'; 
            break; 
        case 2:
            var became = 'Acknowledged'; 
            break; 
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            var became = 'Cleared'; 
            break; 
        default:
            var became = '(unknown)'; 
    } 
    alarm_data_normalized = {
        "when_alarmed":Date.parse(timeAck), 
        "who":who, 
        "alarmed_data":alarmed_data, 
        "acknowledged_data":acknowledged_data, 
        "cleared_data":cleared_data, 
        "became":became, 
        "because": because, 
        "uuid": uuid, 
    };

    return alarm_data_normalized; 
}

exports.normalize_alarms_historical = function(tags) {
    var tags_normalized = []; 
    for(tag of tags){
        var tag_normalized = exports.normalize_alarm_historical(tag);
        tags_normalized.push(tag_normalized); 
    }
    debug("normalize_alarms_historical: " + tags_normalized.length);
    return tags_normalized; 
}

function filter_for_escalate_tags(tags){
    var alarms_filtered = []; 
    for(tag of tags){
        var tag_name = tag.who.split('/')[1];
        var escalate_tag = tag_name.toLowerCase() == 'escalate';
        if(escalate_tag)
            alarms_filtered.push(tag);
    }
    debug("filter_for_escalate_tags: " + alarms_filtered.length);
    return alarms_filtered;
}

function filter_for_operator_actions_or_sys_events(tags){
    var tags_filtered = []; 
    for(tag of tags){
        if(tag.who.indexOf('/') == -1){
            tags_filtered.push(tag);
        } else {
            var tag_path = tag.who.split(':')[3];
            var tag_name = tag_path.split('/')[1];
            var building_tag = tag_name.toLowerCase() != 'syserr';
            if(building_tag)
                tags_filtered.push(tag);
        }
    };
    return tags_filtered;
}

exports.filter_for_operator = function(tags) {
    var alarms_filtered = []; 
    for(tag of tags){
        var alarming = tag.alarming;
        if((alarming)){
            alarms_filtered.push(tag); 
        }
    }
    debug("filter_for_operator: " + alarms_filtered.length);
    return alarms_filtered;
}
function filter_for_public_table(tags) {
    var tags_filtered = [];
    for(tag of tags){
        var escalated = tag.escalated;
        var recently_changed = tag.recently_changed;
        if(escalated || recently_changed){
            tags_filtered.push(tag); 
        }
    }
    debug("normalize_alarms_from_events: " + tags_filtered.length);
    return tags_filtered; 
}

function add_age(tags){
    var current_time = Date.now();
    tags.forEach(function(tag){
        if(tag["alarming"]){
            var elapsed_time = Date.now() - tag.when_alarmed;
        } else {
            var elapsed_time = Date.now() - tag.when_cleared;
        }
        tag.age = elapsed_time;
    });
    debug("add_age: " + tags.length);
    return tags; 
}

function is_alarm_recently_changed(tag) {
    var elapsed_time;
    if(tag["alarming"]){
        elapsed_time = Date.now() - tag["when_alarmed"];
    } else {
        elapsed_time = Date.now() - tag["when_cleared"];
    }
    var recently_changed = elapsed_time < 10 * 60 * 1000;
    return recently_changed;
}

function is_alarm_state_changed_recently(tags){
    for(ndx in tags){
        tags[ndx].recently_changed = is_alarm_recently_changed(tags[ndx]);
    }
    debug("is_alarm_state_changed_recently: " + tags.length);
    return tags;
}

function add_full_name(tag){
    tag.full_name = full_names[tag["who"].split("/")[1].split(':')[1].toLowerCase()];
    return tag;    
}

function add_full_names(tags){
    var tags_out = []; 
    for(tag of tags){
        tag = add_full_name(tag);
        tags_out.push(tag); 
    }; 
    return tags_out   
}

function filter_for_testing(data){
    var rows = data.rows;
    var columns = data.columns;
    var rows_filtered = []; 
    for(row of rows){
        var uuid = row[5];
        if(uuid == "24928f55-4be7-44b3-85b3-b34d4a944cde"){
            rows_filtered.push(row);
        }
    }
    return {rows:rows_filtered, cols:columns};
}

exports.acknowledge = function(uuid,user,notes){
    var body = "";

    const postData = uuid;

    const options = {
      hostname: ignition_machine,
      port: ignition_port,
      path: ignition_alarms_path+"?uuid="+uuid+"&user="+user+"&notes="+notes,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        // 'Content-Length': Buffer.byteLength(postData)
      }
    };

    const req = http.request(options, function(res) {
        res.setEncoding('utf8');
        res.on("data", data => {
            body += data;
        });
        res.on('end', () => {
        // response from ignition ended
        });
    });

    req.on('error', (e) => {
      console.error(`problem with request: ${e.message}`);
    });

    // req.write(postData);
    req.end();

    return 'ok';
}