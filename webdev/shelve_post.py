	logger = system.util.getLogger("webdevShelveAlarm")
	logger.info(': )') 
	path = request['params']['path']
	logger.info('path: ' + str(path))
	time = request['params']['time']
	# path = path.split('/')
	# source_path = ["prov:default:/tag:MAI/Test:/alm:Alarm"]
	# path = "prov:default:/tag:" + path[0] + "/" + path[1] + ":/alm:" + path[2]
	paths = [path]
	system.alarm.shelve(path=paths,timeoutSeconds=time)
	shelved_paths = system.alarm.getShelvedPaths()
	return {'response': "{'result':+"+str(shelved_paths)+"+}", "contentType":"application/json"}