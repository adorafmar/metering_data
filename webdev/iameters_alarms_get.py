	logger = system.util.getLogger("webdevGetAlarms")
	# logger.info(str(alarm.getSource()) + ': ' + str(alarm.getLastEventState()))
	alarms = system.alarm.queryStatus(State=["ActiveUnacked","ActiveAcked","ClearUnacked","ClearAcked"], includeShelved="false")
	alarms_array = []
	for alarm in alarms:
 		# logger.info("for loop15")
		alarm_object = {}
		alarm_object["name"] = alarm.getName()
		alarm_object["who"] = alarm.getSource().toString()
		alarm_object["uuid"] = alarm.getId()
		alarm_object["alarming"] = alarm.getState().isActive()
		alarm_object["acknowledged"] = alarm.getState().isAcked()
		alarm_object["last_event_state"] = alarm.getLastEventState()
		alarm_object["cleared"] = alarm.isCleared()
#		if alarm_object["uuid"].toString() == "3d4eb903-b631-46cb-bf19-641975981efd":
#			logger.info(str(alarm_object))
		event_object = alarm.getActiveData()
		if str(event_object) != 'None':
			event_hash = {}
			for prop in event_object.getProperties():
#				logger.info('getActiveData prop: ' + str(prop))
				prop_name = prop.getName()
				if prop_name == 'mode':
					prop_value = event_object.get(prop).toString()
					event_hash["mode"] = prop_value
				if prop_name == 'eventTime':
					prop_value = event_object.get(prop).getTime()
					event_hash["time"] = prop_value
				if prop_name == 'setpointA':
					prop_value = str(event_object.get(prop))
					event_hash["setpointA"] = prop_value
				if prop_name == 'eventValue':
					prop_value = str(event_object.get(prop))
					event_hash["eventValue"] = prop_value
#					logger.info('getActiveData prop_value: ' + prop_value)
			alarm_object["active_data"] = event_hash
			
		event_object = alarm.getAckData()
		if str(event_object) != 'None':
			event_hash = {}
			for prop in event_object.getProperties():
 				# logger.info('getAckData prop: ' + str(prop))
				prop_name = prop.getName()
				if prop_name == 'eventTime':
					prop_value = event_object.get(prop).getTime()
					event_hash["time"] = prop_value
				if prop_name == 'ackUser':
					try:
						prop_value = event_object.get(prop).toString()
						event_hash["user"] = prop_value
					except:
						logger.info(alarm_object["who"])
						logger.info("An exception occurred: " + str(event_object.get(prop)))				
						event_hash["user"] = "unknown"
				if prop_name == 'ackNotes':
					prop_value = event_object.get(prop)
					event_hash["notes"] = prop_value					
			alarm_object["acknowledged_data"] = event_hash
			
		event_object = alarm.getClearedData()
		if str(event_object) != 'None':
			event_hash = {}
			for prop in event_object.getProperties():
#				logger.info('getClearedData prop: ' + str(prop))
				prop_name = prop.getName()
				if prop_name == 'eventTime':
					prop_value = event_object.get(prop).getTime()
					event_hash["time"] = prop_value
			alarm_object["cleared_data"] = event_hash

		alarms_array.append(alarm_object)
	response = {}
	response["ent_opc_connection_state"] = system.opc.getServerState("ENT-OPC")
	response["alarms"] = alarms_array
	return {'json': response}