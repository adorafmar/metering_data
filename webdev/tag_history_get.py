	logger = system.util.getLogger("webdevGetTagHistory")
	logger.info(': ) ')
	tag_for_ia_journal = str(request["servletRequest"].getParameter("tag_for_ia_journal"))
	logger.info('tag_for_ia_journal: ' + tag_for_ia_journal)
	timeNow = system.date.now()
	timeBefore = system.date.addDays(timeNow,-366)
	sources = ['*'+tag_for_ia_journal+'*']
	alarms = system.alarm.queryJournal(
	   startDate=timeBefore, 
	   endDate=timeNow,
	   source=sources,
	   includeData=True,
	   includeSystem=True
	)
	columns = ['getSource','getActiveData','getAckData','getClearedData','getNotes','getId'] 
	rows = []
	for alarm in alarms:
		row = []
		row.append(str(alarm.getSource()))
		row.append(str(alarm.getActiveData()))
		row.append(str(alarm.getAckData()))
		row.append(str(alarm.getClearedData()))
		row.append(str(alarm.getNotes()))
		row.append(str(alarm.getId()))
		rows.append(row)
	result = {}
	result['columns'] = columns
	# logger.info('alarms len: ' + str(len(rows)))
	result['rows'] = rows
	return {'json': result}	